

  // function printMousePos(event) {
  //   document.body.textContent =
  //     "clientX: " + event.clientX +
  //     " - clientY: " + event.clientY;
  // }


//this allows me to get all of the div tags in a page and stores them in an array
// let divs = document.getElementsByTagName("div");
// var arr = Array.prototype.slice.call( divs );
//
// var i;
// for (i = 0; i < arr.length; i++) {
//     console.log(arr[i].id);
// }
//-------------------------------------------------------------------



//This keeps track of the input forms.
var inputs, index;

inputs = document.getElementsByTagName('input');
console.log("There are: "+inputs.length+" input forms.");
for (index = 0; index < inputs.length; ++index) {
    // console.log(inputs[index]);
    inputs[index].onblur = function () {
      console.log("The user just was on the form and filled out: " + this.value);
      console.log("The input type was: " + this.type);
    };
}



//This keeps track of the buttons that were clicked
var buttons = document.getElementsByTagName('button');
console.log("There are: "+buttons.length+" buttons.");
for (var i = 0; i < buttons.length; i++) {
    var button = buttons[i];
    buttons[i].onclick = function () {
      console.log("The button that says " + this.value +" was just clicked.");

    };

}
